package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCrud11Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoCrud11Application.class, args);
	}

}
