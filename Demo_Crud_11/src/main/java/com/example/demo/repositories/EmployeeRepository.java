package com.example.demo.repositories;

import java.util.List;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	Employee getEmployeeByCity(long emp_city);

	Optional<List<Employee>> findByEmpageGreaterThan(int emp_age);

}
