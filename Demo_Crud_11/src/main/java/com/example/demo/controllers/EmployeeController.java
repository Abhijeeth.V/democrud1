package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.repositories.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	EmployeeRepository employeeRepository;

	// Saving the data into the table
	// @RequestBody -Whatever we are sending in the body that mapping has to be
	// there with object
	// PostMapping

	@PostMapping("/employees")
	public String createNewEmpolyee(@RequestBody Employee employee) {

		employeeRepository.save(employee);
		return "Employee Created in Database";

	}

	// in Get all employee we need to use Response Entity list as its return the
	// list of Employees
	// to store that list of values we need to store that list into new list
	// as return type is Response Entity so we have return that as list
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		List<Employee> empList = new ArrayList<>();
		employeeRepository.findAll().forEach(empList::add);
		return new ResponseEntity<List<Employee>>(empList, HttpStatus.OK);

	}

	// get the employee by employee id
	@GetMapping("/employees/{emp_id}")
	public ResponseEntity<Employee> getemployeeById(@PathVariable long emp_id) {
		Optional<Employee> emp = employeeRepository.findById(emp_id);
// optional is an interface if we do not have value in database then it throws the null pointer exception
		if (emp.isPresent()) {
			return new ResponseEntity<Employee>(emp.get(), HttpStatus.FOUND);

		} else {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}

	}

	// In below code we are updating the data based on the employee ID
	// so whatever the data that we have in employee object that we are getting data
	// from repository we are getting the data and
	// the storing in new variable /or updating the by setting the values and saving
	// the data
	@PutMapping("/employees/{emp_id}")
	public String updateEmployeeById(@PathVariable long emp_id, @RequestBody Employee employee) {
		Optional<Employee> emp = employeeRepository.findById(emp_id);

		if (emp.isPresent()) {

			Employee empExists = emp.get();
			empExists.setEmp_name(employee.getEmp_name());
			empExists.setEmp_salary(employee.getEmp_salary());
			empExists.setEmp_age(employee.getEmp_age());
			empExists.setEmp_city(employee.getEmp_city());

			employeeRepository.save(empExists);
			return "Employee Deatils Against:" + emp_id + "Updated";

		} else {
			return "Employee Deatils does not exists for emp_id:" + emp_id;

		}

	}

	@DeleteMapping("/employees/{emp_id}")
	public String deleteEmployeeById(@PathVariable long emp_id) {
		employeeRepository.deleteById(emp_id);
		return "Employee Deleted Successfully";

	}

	// below method is to delete all the employees from the table

	@DeleteMapping("/employees")
	public String deleteAllEmpolyee() {
		employeeRepository.deleteAll();
		return " All Employee Deleted Successfully";

	}

	@GetMapping("/employees/{emp_city}")
	public ResponseEntity<Employee> getAllEmployeeByCity(@RequestParam long emp_city) {
		Employee emp = employeeRepository.getEmployeeByCity(emp_city);

		return new ResponseEntity<Employee>(emp, HttpStatus.FOUND);
	}

	
	
	//  below is the logic for get all the employees whose age is more the age passed in request 
	@GetMapping("/employee/employeeGreaterThan")
	public ResponseEntity<List<Employee>> getEmployeeGreaterThan(@RequestParam("emp_age") int emp_age){
		Optional<List<Employee>> empList = employeeRepository.findByEmpageGreaterThan(emp_age);
		return new ResponseEntity<List<Employee>>(empList.get(), HttpStatus.FOUND);
	}
	
}
